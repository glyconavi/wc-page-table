## GlycoNavi Data Table

displays data retrieved from glyconavi.

### Paging

paging-compatible using {{ page }} and {{ limit }} data variables.

### Demo

Visit demo/index.html to see live examples.
